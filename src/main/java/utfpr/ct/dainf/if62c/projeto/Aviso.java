package utfpr.ct.dainf.if62c.projeto;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimerTask;

/**
 * Linguagem Java
 * @author 
 */
public class Aviso extends TimerTask {
    
    protected final Compromisso compromisso;

    private final Date currentTime = new Date();    
    protected SimpleDateFormat sdf;
    
    public Aviso(Compromisso compromisso) {
        this.compromisso = compromisso;
        sdf = new SimpleDateFormat("'"+compromisso.getDescricao()+" começa em 'ss's'");
    }
    
    @Override
    public void run() {        
        currentTime.setTime(System.currentTimeMillis());
        System.out.println(sdf.format(currentTime));
        cancel();
    }
    
}