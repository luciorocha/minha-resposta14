package utfpr.ct.dainf.if62c.projeto;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Timer;

/**
/**
 * Linguagem Java
 * @author 
 */
public class Agenda {
    private final String descricao;
    private final List<Compromisso> compromissos = new ArrayList<>();
    private final List<Aviso> avisos = new ArrayList<>();
    private final Timer timer;
    
    public Agenda(String descricao) {
        this.descricao = descricao;
        timer = new Timer(descricao);
    }

    public String getDescricao() {
        return descricao;
    }

    public List<Compromisso> getCompromissos() {
        return compromissos;
    }
    
    public List<Aviso> getAvisos() {
        return avisos;
    }
    
    public void novoCompromisso(Compromisso compromisso) {
        compromissos.add(compromisso);
        Aviso aviso = new AvisoFinal(compromisso);
        compromisso.registraAviso(aviso);
        // com a classe Aviso devidamente implementada, o erro de compilação
        // deverá desaparecer
        timer.schedule(aviso, compromisso.getData());
    }
    
    public void novoAviso(Compromisso compromisso, int antecedencia) {
        

    }
    
    public void novoAviso(Compromisso compromisso, int antecedencia, int intervalo) {
                        
        Aviso aviso = new Aviso(compromisso);
        avisos.add(aviso);
                
        timer.scheduleAtFixedRate(aviso, antecedencia, intervalo);                        
        
    }
    
    public void cancela(Compromisso compromisso) {

        if (compromissos.contains(compromisso)){
            compromissos.remove(compromisso);
        }
        
    }
    
    public void cancela(Aviso aviso) {
    
        if (avisos.contains(aviso))
            avisos.remove(aviso);
        
    }
    
    public void destroi() {
    
        List<Compromisso> listaCompromissos = getCompromissos();
        for (Compromisso item : listaCompromissos){
           cancela(item);
        }
        List<Aviso> listaAvisos = getAvisos();
        for (Aviso item : listaAvisos){
           cancela(item);
        }
    
        
    }
}
