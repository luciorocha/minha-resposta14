package utfpr.ct.dainf.if62c.projeto;

import java.text.SimpleDateFormat;

/**
 * Linguagem Java
 * @author 
 */
public class AvisoFinal extends Aviso {

    public AvisoFinal(Compromisso compromisso) {                     
        super(compromisso);        
        sdf = new SimpleDateFormat("'"+compromisso.getDescricao()+" começa agora'");
    }
    
}
